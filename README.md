# WateringApp-backend 1.0.0
## Getting started

This is the backend part of our incoming application, WateringApp.

This application is destined to help with the complexity of nurturing plants at home. Should show some information about watering, next watering date, how urgent it is to do it per plant etc.

## Usage
```
git clone https://gitlab.com/wetbois/wateringapp/wateringapp-backend.git mvn clean install
```
After these you should be able to reach the endpoints through the localhost:8080

### API
(Planning to deploy api documentation using SWAGGER)


### Contributors:
- Mészáros Dániel
- Szalontai Márk
- Ágoston Tamás
- Varga Gergely

WetBois / WateringApp / wateringapp-backend
GitLab.com
