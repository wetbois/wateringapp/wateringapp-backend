package com.wetbois.wateringapp.controller;

import com.wetbois.wateringapp.dto.PlantDto;
import com.wetbois.wateringapp.entity.Plant;
import com.wetbois.wateringapp.service.PlantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;

@RestController
@Transactional
@RequestMapping(value = "/plants")
public class PlantController {

	@Autowired
	PlantService plantService;

	@GetMapping("/all")
	public List<PlantDto> getAllPlantsDto() {
		return plantService.getAllPlantsDto();
	}

	@GetMapping("/{id}")
	public PlantDto getPlantDtoByPlantId(@PathVariable("id") Long id) {
		return plantService.getPlantDtoByPlantId(id);
	}

	@PostMapping("/watering/{id}")
	public void wateringAPlant(@PathVariable("id") Long id){
		plantService.wateringAPlant(id);
	}

	@PostMapping("/createPlant")
	public void createPlant(@RequestBody Plant plant){
		plantService.createPlant(plant);
	}

	@PostMapping("/edit/{id}")
	public void editPlant(@PathVariable("id") Long id, @RequestBody Plant plant) {
		plantService.editPlant(id, plant);
	}

}
