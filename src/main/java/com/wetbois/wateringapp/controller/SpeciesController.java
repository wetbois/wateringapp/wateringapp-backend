package com.wetbois.wateringapp.controller;

import com.wetbois.wateringapp.entity.Species;
import com.wetbois.wateringapp.service.SpeciesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;

@RestController
@Transactional
@RequestMapping(value = "/species")
public class SpeciesController {
    @Autowired
    SpeciesService speciesService;

    @GetMapping("/all")
    public List<Species> getAllSpecies() {
        return speciesService.getAllSpecies();
    }

    @GetMapping("/{id}")
    public Species getSpeciesById(@PathVariable("id") Long id) {
        return speciesService.getSpeciesById(id);
    }

    @PostMapping("/createSpecies")
    public void createSpecies(@RequestBody Species species){
        speciesService.createSpecies(species);
    }

    @PostMapping("/edit/{id}")
    public void editSpecies(@PathVariable("id") Long id, @RequestBody Species species) {
        speciesService.editSpecies(id, species);
    }
}
