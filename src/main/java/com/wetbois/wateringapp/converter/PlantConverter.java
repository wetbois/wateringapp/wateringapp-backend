package com.wetbois.wateringapp.converter;

import com.wetbois.wateringapp.dto.PlantDto;
import com.wetbois.wateringapp.entity.Plant;
import com.wetbois.wateringapp.entity.Species;
import com.wetbois.wateringapp.service.SpeciesService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import static lombok.Lombok.checkNotNull;

@Service
@RequiredArgsConstructor
public class PlantConverter {

    @Autowired
    SpeciesService speciesService;

    public PlantDto toDTO(Plant plant) {
        checkNotNull(plant, "ENTITY_CANNOT_BE_NULL");

        Species species = speciesService.getSpeciesById(plant.getSpeciesId());

        Date now = new Date();
        LocalDateTime tempLocalDateTimeForAdd = LocalDateTime.ofInstant(plant.getLastWateringDate().toInstant(), ZoneId.systemDefault());
        Date nextWateringDate = Date.from(tempLocalDateTimeForAdd.plusDays(species.getWateringFrequency()).atZone(ZoneId.systemDefault()).toInstant());
        if(nextWateringDate.compareTo(now) < 0) {
            nextWateringDate = now;
        }

        Long betweenDays = getDifferenceDays(plant.getLastWateringDate(), now);
        Integer waterPercent = 100 - Math.round(((float)betweenDays/(float)species.getWateringFrequency())*100);
        if(waterPercent < 0) waterPercent = 0;

        return PlantDto.builder()
                .imageUrl(species.getImageUrl())
                .nextWateringDate(nextWateringDate)
                .nickName(plant.getNickName())
                .speciesName(species.getSpeciesName())
                .waterPercentage(waterPercent)
                .waterQuantity(species.getWaterQuantity())
                .build();
    }

    public List<PlantDto> toDTOMass(List<Plant> plantList){
        List<PlantDto> allPlantDto = new ArrayList<>();
        for(Plant plant : plantList){
            allPlantDto.add(toDTO(plant));
        }
        return allPlantDto;
    }

    public Long getDifferenceDays(Date d1, Date d2) {
        long diff = d2.getTime() - d1.getTime();
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }

}
