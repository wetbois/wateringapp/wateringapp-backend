package com.wetbois.wateringapp.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PlantDto {

    private Integer waterQuantity;

    private String speciesName;

    private String imageUrl;

    private String nickName;

    private Date nextWateringDate;

    private Integer waterPercentage;

}
