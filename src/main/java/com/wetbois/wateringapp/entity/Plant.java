package com.wetbois.wateringapp.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name="plants")
public class Plant {

	@Id
	@GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	private Long id;

	@Column(name = "nickname", nullable = false)
	private String nickName;

	@Column(name = "last_watering_date")
	private Date lastWateringDate;

	@Column(name = "species_id", nullable = false)
	private Long speciesId;

}
