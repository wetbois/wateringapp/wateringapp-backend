package com.wetbois.wateringapp.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name="species")
public class Species {

	@Id
	@GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	private Long id;

	@Column(name = "species_name")
	private String speciesName;

	@Column(name = "image_url")
	private String imageUrl;

	@Column(name = "water_quantity", nullable = false)
	private Integer waterQuantity;

	@Column(name = "watering_frequency", nullable = false)
	private Integer wateringFrequency;

}
