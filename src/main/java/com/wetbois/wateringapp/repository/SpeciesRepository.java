package com.wetbois.wateringapp.repository;

import com.wetbois.wateringapp.entity.Species;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SpeciesRepository extends JpaRepository<Species, Long> {
}
