package com.wetbois.wateringapp.service;

import com.wetbois.wateringapp.converter.PlantConverter;
import com.wetbois.wateringapp.dto.PlantDto;
import com.wetbois.wateringapp.entity.Plant;
import com.wetbois.wateringapp.repository.PlantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class PlantService {

	@Autowired
	PlantRepository plantRepository;

	@Autowired
	PlantConverter plantConverter;

	public List<Plant> getAllPlants(){
		return plantRepository.findAll();
	}

	public Plant getPlantById(Long id){
		return plantRepository.findById(id).get();
	}

	public List<PlantDto> getAllPlantsDto(){
		return plantConverter.toDTOMass(getAllPlants());
	}

	public PlantDto getPlantDtoByPlantId(Long id){
		return plantConverter.toDTO(getPlantById(id));
	}

	public void wateringAPlant(Long id){
		Plant plant = getPlantById(id);
		plant.setLastWateringDate(new Date());
		plantRepository.save(plant);
	}

	public void createPlant(Plant plant){
		plantRepository.save(plant);
	}

	public void editPlant(Long id, Plant plant){
		Plant editPlant = getPlantById(id);
		editPlant.setNickName(plant.getNickName());
		editPlant.setSpeciesId(plant.getSpeciesId());
		editPlant.setLastWateringDate(plant.getLastWateringDate());
		plantRepository.save(editPlant);
	}

}
