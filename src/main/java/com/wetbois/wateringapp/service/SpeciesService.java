package com.wetbois.wateringapp.service;

import com.wetbois.wateringapp.entity.Species;
import com.wetbois.wateringapp.repository.SpeciesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SpeciesService {

    @Autowired
    SpeciesRepository speciesRepository;

    public List<Species> getAllSpecies(){
        return speciesRepository.findAll();
    }

    public Species getSpeciesById(Long id){
        return speciesRepository.findById(id).get();
    }

    public void createSpecies(Species species){
        speciesRepository.save(species);
    }

    public void editSpecies(Long id, Species species){
        Species editSpecies = getSpeciesById(id);
        editSpecies.setSpeciesName(species.getSpeciesName());
        editSpecies.setImageUrl(species.getImageUrl());
        editSpecies.setWateringFrequency(species.getWateringFrequency());
        editSpecies.setWaterQuantity(species.getWaterQuantity());
        speciesRepository.save(editSpecies);
    }

}
