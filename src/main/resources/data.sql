INSERT INTO species (id, species_name, image_url, water_quantity, watering_frequency) VALUES (1, 'Rózsa', 'https://cdn.pixabay.com/photo/2016/05/28/18/25/flower-1421903__340.jpg', 10, 15);
INSERT INTO species (id, species_name, image_url, water_quantity, watering_frequency) VALUES (2, 'Tulipán', 'https://urbanjunglebudapest.hu/wp-content/uploads/2019/11/aaron-burden-dTqhC_kroMc-unsplash-683x1024.jpg', 3, 5);
INSERT INTO species (id, species_name, image_url, water_quantity, watering_frequency) VALUES (3, 'Fű', 'https://images.landscapingnetwork.com/pictures/images/675x529Max/site_8/fescue-shutterstock_10745.jpg', 19, 30);
INSERT INTO plants (id, nickname, last_watering_date, species_id) VALUES (1, 'Rozsa1', '2022-04-01', 1);
INSERT INTO plants (id, nickname, last_watering_date, species_id) VALUES (2, 'Tulipán1', '2022-04-01', 2);
INSERT INTO plants (id, nickname, last_watering_date, species_id) VALUES (3, 'Tulipán2', '2022-03-17', 2);
INSERT INTO plants (id, nickname, last_watering_date, species_id) VALUES (4, 'Fű1', '2022-03-17', 3);
INSERT INTO plants (id, nickname, last_watering_date, species_id) VALUES (5, 'Fű2', '2022-04-02', 3);
INSERT INTO plants (id, nickname, last_watering_date, species_id) VALUES (6, 'Fű3', '2022-02-22', 3);