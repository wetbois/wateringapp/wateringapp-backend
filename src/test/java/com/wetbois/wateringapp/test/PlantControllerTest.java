package com.wetbois.wateringapp.test;

import com.wetbois.wateringapp.controller.PlantController;
import com.wetbois.wateringapp.dto.PlantDto;
import com.wetbois.wateringapp.service.PlantService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(PlantController.class)
public class PlantControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private PlantService plantService;

    @Test
    public void getAllPlantsDtoTest() throws Exception {
        PlantDto plantDto = new PlantDto();
        plantDto.setNickName("Rózsa");

        List<PlantDto> allPlantDtos = Arrays.asList(plantDto);

        given(plantService.getAllPlantsDto()).willReturn(allPlantDtos);

        mvc.perform(get("/plants/all")
                        .contentType(MediaType.APPLICATION_JSON))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$", hasSize(1)))
                        .andExpect(jsonPath("$[0].nickName", is(plantDto.getNickName())));
    }

    @Test
    public void getPlantDtoByPlantIdTest() throws Exception {
        PlantDto plantDto = new PlantDto();
        plantDto.setNickName("Rózsa");

        given(plantService.getPlantDtoByPlantId(1L)).willReturn(plantDto);

        mvc.perform(get("/plants/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("nickName", is(plantDto.getNickName())));
    }



}
