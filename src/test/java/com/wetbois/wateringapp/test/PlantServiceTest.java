package com.wetbois.wateringapp.test;

import com.wetbois.wateringapp.entity.Plant;
import com.wetbois.wateringapp.repository.PlantRepository;
import com.wetbois.wateringapp.service.PlantService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PlantServiceTest {

    @Mock
    private PlantRepository plantRepository;

    @InjectMocks
    private PlantService plantService;

    @Test
    public void getPlantByIdTest() {
        Plant plant = new Plant();
        plant.setId(1L);

        when(plantRepository.findById(plant.getId())).thenReturn(Optional.of(plant));

        Plant expected = plantService.getPlantById(plant.getId());

        assertThat(expected).isSameAs(plant);
        verify(plantRepository).findById(plant.getId());
    }

    @Test(expected = NoSuchElementException.class)
    public void getPlantByIdDoesntExistTest() {
        Plant plant = new Plant();
        plant.setId(10L);

        given(plantRepository.findById(anyLong())).willReturn(Optional.ofNullable(null));
        plantService.getPlantById(plant.getId());
    }

    @Test
    public void getAllPlantsTest() {
        List<Plant> plants = new ArrayList();
        plants.add(new Plant());

        given(plantRepository.findAll()).willReturn(plants);

        List<Plant> expected = plantService.getAllPlants();

        assertEquals(expected, plants);
        verify(plantRepository).findAll();
    }

}
