package com.wetbois.wateringapp.test;

import com.wetbois.wateringapp.entity.Species;
import com.wetbois.wateringapp.repository.SpeciesRepository;
import com.wetbois.wateringapp.service.SpeciesService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SpeciesServiceTest {

    @Mock
    private SpeciesRepository speciesRepository;

    @InjectMocks
    private SpeciesService speciesService;

    @Test
    public void getSpeciesByIdTest() {
        Species species = new Species();
        species.setId(1L);

        when(speciesRepository.findById(species.getId())).thenReturn(Optional.of(species));

        Species expected = speciesService.getSpeciesById(species.getId());

        assertThat(expected).isSameAs(species);
        verify(speciesRepository).findById(species.getId());
    }

    @Test(expected = NoSuchElementException.class)
    public void getSpeciesByIdDoesntExistTest() {
        Species species = new Species();
        species.setId(10L);

        given(speciesRepository.findById(anyLong())).willReturn(Optional.ofNullable(null));
        speciesService.getSpeciesById(species.getId());
    }

    @Test
    public void getAllSpeciesTest() {
        List<Species> species = new ArrayList();
        species.add(new Species());

        given(speciesRepository.findAll()).willReturn(species);

        List<Species> expected = speciesService.getAllSpecies();

        assertEquals(expected, species);
        verify(speciesRepository).findAll();
    }

}
